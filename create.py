import sqlite3
import psutil
import platform
import sys
import subprocess
import os
import peewee
from sqlite3 import Error
import script
from script import scripts
from tables import Host, Hd, HdHistory
import requests
import simplejson

#Atribuindo valores para o HOST
hostname = platform.node()
sistema = platform.system()
plataforma = platform.version()
cpu_qtd = psutil.cpu_count()
mem_total = psutil.virtual_memory().total
processador = scripts.getNameProcessor()

url = "http://192.168.200.183:8000/api/host"

dataHost = {'nome': hostname,    
        'cliente_id': 1,    
        'plataforma': plataforma,    
        'os': sistema,    
        'tipo': 'Notebook',    
        'cpu': processador,    
        'cpu_core': cpu_qtd,    
        'mem_ram': mem_total}
        
payloadHost = simplejson.dumps(dataHost)

headers = {
    'Content-Type': "application/json",
    'X-Requested-With': "XMLHttpRequest",
    'cache-control': "no-cache",
    'Postman-Token': "c658ca5a-13a8-4641-9ff1-62952bfd36c8" }

#Inserir HOST
def insertHost(idhost, idcliente, type):
    #Tratamento de erro ao insrir dados
    try:
        #Inserindo HOST
        host = Host.create(
                        idhost = idhost, 
                        hostname = hostname, 
                        plataform = sistema, 
                        os = plataforma, 
                        client_id = idcliente, 
                        type = type, 
                        cpu = processador, 
                        core_cpu = cpu_qtd, 
                        mem_ram = mem_total)    
        print("Host inserido com sucesso!")
        insertHd(idhost)
    except peewee.IntegrityError:
        print("Erro ao inserir, ID HOST já existe!")

#Inserir HD
def insertHd(idhost):
    try:
        #Atribuindo valores para o HD
        disks = psutil.disk_partitions()
        for i in disks:
            #Pular discos irrelevantes
            if i.opts == "cdrom" or i.fstype == "squashfs":
                pass
            else:
                #Atribuindo valores para o HD_HISTORY
                used = psutil.disk_usage(i.mountpoint).used
                avaible = psutil.disk_usage(i.mountpoint).free
                percent = psutil.disk_usage(i.mountpoint).percent
                path = i.mountpoint 
                capacity = psutil.disk_usage(i.mountpoint).total
                dataHd = {'nome': path,
                        'capacidade': capacity,
                        'host_id': idhost}      
                payloadHd = simplejson.dumps(dataHd)
                url = "http://192.168.200.183:8000/api/hd"
                response = requests.request("POST", url, data=payloadHd, headers=headers)
                responseJSON = simplejson.loads(response.text)
                #Inserindo HD
                hd = Hd.create(
                            idhd = responseJSON['id'],
                            path = path, 
                            capacity = capacity, 
                            idhost = idhost)
            print("HD inserido com sucesso!")
            #Inserindo HD_HISTORY
            insertHdHistory(hd.idhd, used, avaible, percent)
    except peewee.IntegrityError:
        print("Erro ao inserir HD!")

#Inserir HD_HISTORY
def insertHdHistory(idhd, used, avaible, percent):
    try:
        hdHistory = HdHistory.create(
                                    idhd = idhd,
                                    used = used,
                                    avaible = avaible,
                                    percent = percent)
        print("HD_HISTORY inserido com sucesso!")

    except peewee.IntegrityError:
        print("Erro ao inserir HD!")

def main():
    response = requests.request("POST", url, data=payloadHost, headers=headers)

    #print(payloadHost)
    responseJSON = simplejson.loads(response.text)
    print(responseJSON['id']) 
    
    #insertHost(idhost, idcliente, type)
    insertHost(responseJSON['id'], '1', 'Notebook')

if __name__ == '__main__':
    main()
