import requests

url = "http://192.168.200.183:8000/api/host"

payload = "{\n    \"nome\": \"Teste\",\n    \"cliente_id\": 1,\n    \"plataforma\": \"Linux\",\n    \"os\": \"Ubuntu\",\n    \"tipo\": \"Notebook\",\n    \"cpu\": \"Intel\",\n    \"cpu_core\": 8,\n    \"mem_ram\": 8092\n    \n}"
headers = {
    'Content-Type': "application/json",
    'X-Requested-With': "XMLHttpRequest",
    'cache-control': "no-cache",
    'Postman-Token': "c658ca5a-13a8-4641-9ff1-62952bfd36c8"
    }

response = requests.request("POST", url, data=payload, headers=headers)

print(response.text)