import peewee
import datetime

#Criando o banco de dados
db = peewee.SqliteDatabase('host.db')

class BaseHost(peewee.Model):

    class Meta:
        database = db

#Tabela Host
class Host(BaseHost):
    idhost = peewee.BigIntegerField(primary_key = True, unique = True)
    hostname = peewee.TextField(null = False)
    plataform = peewee.TextField(null = False)
    os = peewee.TextField(null = False)
    client_id = peewee.BigIntegerField(null = False)
    type = peewee.TextField(null = False)
    cpu = peewee.TextField(null = False)
    core_cpu = peewee.BigIntegerField(null = False)
    mem_ram = peewee.FloatField(null = False)

#Tabela Hd
class Hd(BaseHost):
    idhd = peewee.BigIntegerField(primary_key = True, unique = True)
    path = peewee.TextField(null = False)
    capacity = peewee.FloatField(null = False)
    idhost = peewee.ForeignKeyField(Host)

class HdHistory(BaseHost):
    idhd = peewee.BigAutoField(primary_key = True)
    idcloud = peewee.BigIntegerField(null = True)
    used = peewee.FloatField(null = False)
    avaible = peewee.FloatField(null = False)
    percent = peewee.FloatField(null = False)
    created_at = peewee.DateTimeField(default=datetime.datetime.now)
    updated_at = peewee.DateTimeField(default=datetime.datetime.now)

#Crinando as tabelas
if __name__ == '__main__':
    try:
        Host.create_table()
        print("Tabela 'Host' criada com sucesso!")
    except peewee.OperationalError:
        print("Tabela 'Host' ja existe!")

    try:
        Hd.create_table()
        print("Tabela 'Hd' criada com sucesso!")
    except peewee.OperationalError:
        print("Tabela 'Hd' ja existe!")
    
    try:
        HdHistory.create_table()
        print("Tabela 'HdHistory' criada com sucesso!")
    except peewee.OperationalError:
        print("Tabela 'HdHistory' ja existe!")
