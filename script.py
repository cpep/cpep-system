
import sys
import psutil
import platform
import subprocess
import os


sistema = platform.system()
plataforma = platform.version()
hostname = platform.node()
release = platform.release()

class scripts():
   # Hostname, Plataforma e versão
   def getHostNamePlatformVersion():
      print("Sistema:", sistema)
      print("Plataforma:", plataforma)
      print("Hostname:", hostname)
      print("Release:", release)
      
   # Versão processador
   def getNameProcessor():
      if sistema == "Linux":
         command = "cat /proc/cpuinfo"
         processador = subprocess.check_output("cat /proc/cpuinfo | head -10 | grep 'model name' | cut -d' ' -f3-20", shell=True)
         #print("Processador:", processador)
         return processador
      elif sistema == "Darwin":   
         os.environ['PATH'] = os.environ['PATH'] + os.pathsep + '/usr/sbin'
         command ="sysctl -n machdep.cpu.brand_string"
         processador = subprocess.check_output(command, shell=True).strip()
         #print("Processador:", processador)
         return processador
      else:
         processador = platform.processor()
         #print("Processador:", processador)
         return processador

   # Uso da CPU
   def getUsageProcessor():
      cpu = psutil.cpu_percent(interval=1)
      print("Uso da CPU:", cpu)

   # Quantidade de cores
   def getQtdProcessor():
      cpu_qtd = psutil.cpu_count()
      print("Quantidade de cores:", cpu_qtd)

   # Memória
   def getMemory():
      mem_total = psutil.virtual_memory().total
      mem_avaiable = psutil.virtual_memory().available
      mem_used = psutil.virtual_memory().used
      mem_percent = psutil.virtual_memory().percent
      print("RAM:", mem_total, "Disponível:", mem_avaiable, "Usada:", mem_used, "Percentual de uso:", mem_percent)
   
   # Usuários conectados
   def getUserConected():    
      nome = psutil.users()
      for i in nome:
         print("Usuário Conectado:", i.name)
   
   # Quantidade e total de discos
   def getQtdDisk():   
      disks = psutil.disk_partitions()
      for i in disks:
         if i.opts == "cdrom" or i.fstype == "squashfs":
            pass
         else:
            print("Partição:", i.mountpoint, "Uso do disco:", psutil.disk_usage(i.mountpoint).total)
   
   # Quantidade de interfaces e endereços
   def getQtdInterfaces():
      net = psutil.net_if_addrs()
      for i in net:
         if i == "lo" or "Loopback" in i:      
            pass
         else:
            for x in net[i]:
               print("Interface:", i, x.address)

def main():
   scripts.getHostNamePlatformVersion()
   print("Processador:", scripts.getNameProcessor())
   scripts.getUsageProcessor()
   scripts.getQtdProcessor()
   scripts.getMemory()
   scripts.getUserConected()
   scripts.getQtdDisk()
   scripts.getQtdInterfaces()

if __name__ == '__main__':
   main()
