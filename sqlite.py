import sqlite3
import psutil
import platform
from sqlite3 import Error



def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)
    return conn


def create_host(conn, host):
    sql = ''' INSERT INTO host(idhost,hostname,plataform,os,client_id,type,cpu,core_cpu,mem_ram) VALUES(?,?,?,?,?,?,?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, host)
    return cur.lastrowid

def select_all_host(conn):
    cur = conn.cursor()
    cur.execute("SELECT * FROM host")
    rows = cur.fetchall()
    return rows


def create_hd(conn, hd):
    sql = ''' INSERT INTO hd(path,capacity,idhost) VALUES(?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, hd)
    return cur.lastrowid

def main1():
    database = r"host.db"
    conn = create_connection(database)
    with conn:
       resut = select_all_host(conn)
       print(len(resut))

def main():
    database = r"host.db"
    conn = create_connection(database)
    with conn:
        # Insert Host
        hostname = platform.node()
        sistema = platform.system()
        plataforma = platform.version()
        cpu_qtd = psutil.cpu_count()
        mem_total = psutil.virtual_memory().total
        host = ('1990', hostname, sistema, plataforma, '1','Notebook', 'Intel i5', cpu_qtd, mem_total)
        host_id = create_host(conn, host)
        print(host_id)

        # Insert HD
        disks = psutil.disk_partitions()
        for i in disks:
            if i.opts == "cdrom" or i.fstype == "squashfs":
                pass
            else:
                hd = (i.mountpoint,psutil.disk_usage(i.mountpoint).total,host_id)
                hd_id = create_hd(conn, hd)

if __name__ == "__main__":
    main()
